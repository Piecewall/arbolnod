/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Orlando Cach
 */
public class ArbolBinario {

    NodoArbol raiz;

    public ArbolBinario() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol nuevo = new NodoArbol(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol auxiliar = raiz;
            NodoArbol padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.dato) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void inOrden(NodoArbol r) {       
        if (r != null) {
            inOrden(r.hijoizquierdo);
            System.out.println(r.dato);
            inOrden(r.hijoderecho);
        }
    }

    public void preOrden(NodoArbol r) {    
        if (r != null) {
            System.out.println(r.dato);
            preOrden(r.hijoizquierdo);
            preOrden(r.hijoderecho);
        }
    }

    public void postOrden(NodoArbol r) {
        if (r != null) {          
            postOrden(r.hijoizquierdo);
            postOrden(r.hijoderecho);
            System.out.println(r.dato);
        }
    }
}
